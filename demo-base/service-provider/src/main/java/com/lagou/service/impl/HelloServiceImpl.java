package com.lagou.service.impl;

import com.lagou.service.HelloService;
import org.apache.dubbo.config.annotation.Service;

import java.util.Random;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/4/20 22:59
 **/
@Service
public class HelloServiceImpl implements HelloService {

    // 随机休眠时间 1-100
    private Random random = new Random();

    @Override
    public String sayHello1(String name) {
        try {
            Thread.sleep(random.nextInt(100));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("hello1");
        return name;
    }

    @Override
    public String sayHello2(String name) {
        try {
            Thread.sleep(random.nextInt(100));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("hello2");
        return name;
    }

    @Override
    public String sayHello3(String name) {
        try {
            Thread.sleep(random.nextInt(100));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("hello3");
        return name;
    }
}
