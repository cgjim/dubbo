package com.lagou.threadpool;

import org.apache.dubbo.common.URL;
import org.apache.dubbo.common.threadpool.support.fixed.FixedThreadPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.*;


public class WatchingThreadPool extends FixedThreadPool implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(WatchingThreadPool.class);

    // 定义线程池使用的阈值
    private static final double ALARM_PERCENT = 0.90;

    private final Map<URL, ThreadPoolExecutor> THREAD_POOLS = new ConcurrentHashMap<>();

    public WatchingThreadPool(){
        Executors.newSingleThreadScheduledExecutor().scheduleWithFixedDelay(this,3, 3,TimeUnit.SECONDS);
    }

    // 通过父类常见线程池


    @Override
    public Executor getExecutor(URL url) {
        Executor executor = super.getExecutor(url);
        if(executor instanceof ThreadPoolExecutor){
            THREAD_POOLS.put(url,(ThreadPoolExecutor)executor);
        }
        return executor;
    }

    // 获取活跃线程数量，总线程数量，计算使用率
    @Override
    public void run() {
        for (Map.Entry<URL, ThreadPoolExecutor> executorEntry : THREAD_POOLS.entrySet()) {
            URL url = executorEntry.getKey();
            ThreadPoolExecutor threadPoolExecutor = executorEntry.getValue();
            int activeCount = threadPoolExecutor.getActiveCount();
            int poolSize = threadPoolExecutor.getCorePoolSize();
            double userPercent = activeCount / (poolSize * 1.0);
            LOGGER.info("线程池执行状态:[{}/{}:{}%]",activeCount,poolSize,userPercent);
            if(userPercent > ALARM_PERCENT){
                LOGGER.error("超出警戒线！host:{} 当前使用率是：{}，URL:{}",url.getIp(),userPercent,url);
            }
        }
    }
}
