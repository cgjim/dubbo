package com;

import com.lagou.bean.ConsumerComponent;
import com.lagou.filter.TimeFilter;
import org.apache.dubbo.common.threadpool.ThreadPool;
import org.apache.dubbo.common.threadpool.support.cached.CachedThreadPool;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.io.IOException;
import java.sql.Time;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/4/20 23:18
 **/
public class AnnotationConsumerMain {
    private static AtomicInteger atomicInteger = new AtomicInteger(0);

    public static void main(String[] args) throws InterruptedException, IOException {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ConsumerConfiguration.class);
        context.start();
        ConsumerComponent bean = context.getBean(ConsumerComponent.class);
//        while (true){
//            System.in.read();
//            System.out.println("result:"+bean.sayHello1("liusong"));
//            for (int i = 0; i < 1000; i++) {
//                Thread.sleep(5);
//                new Thread(()->{
//                    bean.sayHello1("lusong");
//                }).start();
//            }
//        }

        // 每五秒钟答应各方法 TP90 TP99 的耗时情况
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                report("h1",(ArrayList<String>)TimeFilter.h1.clone());
                report("h2",(ArrayList<String>)TimeFilter.h2.clone());
                report("h3",(ArrayList<String>)TimeFilter.h3.clone());
            }
        }, 10000, 5000);

        // 利用线程池 来调用提供者的方法
        ExecutorService pool = Executors.newCachedThreadPool();
        while (true){
            pool.submit(new RunTask(bean));
            atomicInteger.getAndAdd(3);
            Thread.sleep(10);
        }

    }

    public static void report(String methodName,ArrayList<String> h1){
        long timeMillis = System.currentTimeMillis()-60000;
        int index = 0;
        for (String s : h1) {
            if(Long.parseLong(s.split(":")[0]) > timeMillis){
                index = h1.indexOf(s);
                break;
            }
        }
        List<String> subList = h1.subList(index, h1.size());
        if(subList.isEmpty()){
            return;
        }
        int size = subList.size();
        int[] values = new int[size];
        for (int i = 0; i < subList.size(); i++) {
            values[i] = Integer.parseInt(subList.get(i).split(":")[1]);
        }
        Arrays.sort(values);
        int tp90 = (int) (size * 0.9);
        int tp99 = (int) (size * 0.99);
        int tp90Value = values[tp90];
        int tp99Value = values[tp99];
        switch (methodName){
            case "h1":
                System.out.println("方法1的TP90耗时："+tp90Value);
                System.out.println("方法1的TP99耗时："+tp99Value);
                TimeFilter.h1.clear();
                break;
            case "h2":
                System.out.println("方法2的TP90耗时："+tp90Value);
                System.out.println("方法2的TP99耗时："+tp99Value);
                TimeFilter.h2.clear();
                break;
            case "h3":
                System.out.println("方法3的TP90耗时："+tp90Value);
                System.out.println("方法3的TP99耗时："+tp99Value);
                TimeFilter.h3.clear();
                break;
        }

    }

    @Configuration
    @PropertySource("classpath:/dubbo-consumer.properties")
    @ComponentScan(basePackages = "com.lagou.bean")
    @EnableDubbo
    static class ConsumerConfiguration{

    }

    static class RunTask implements Runnable{

        private ConsumerComponent consumerComponent;

        public RunTask(ConsumerComponent consumerComponent){
            this.consumerComponent = consumerComponent;
        }

        /**
         * 调用provide服务的三个方法
         */
        @Override
        public void run() {
            consumerComponent.sayHello1("1");
            consumerComponent.sayHello2("2");
            consumerComponent.sayHello3("3");
        }
    }

}
