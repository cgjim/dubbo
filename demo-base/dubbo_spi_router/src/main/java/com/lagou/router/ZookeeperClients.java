package com.lagou.router;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/4/26 22:56
 **/
public class ZookeeperClients {

    private final CuratorFramework client;

    private static ZookeeperClients INSTANCE;

    static {
        RetryPolicy retry = new ExponentialBackoffRetry(1000, 3);
        CuratorFramework client = CuratorFrameworkFactory.newClient("127.0.0.1", retry);
        INSTANCE = new ZookeeperClients(client);
        client.start();
    }

    private ZookeeperClients(CuratorFramework client){
        this.client = client;
    }

    public static CuratorFramework client(){
        return INSTANCE.client;
    }



}
