package com.lagou.router;

import org.apache.dubbo.common.URL;
import org.apache.dubbo.rpc.Invocation;
import org.apache.dubbo.rpc.Invoker;
import org.apache.dubbo.rpc.RpcException;
import org.apache.dubbo.rpc.cluster.Router;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/4/27 23:08
 **/
public class RestartingInstanceRouter implements Router {

    private final ReadyRestartInstance instance;

    private final URL url;

    public RestartingInstanceRouter(URL url){
        this.url = url;
        this.instance = ReadyRestartInstance.create();
    }


    @Override
    public URL getUrl() {
        return null;
    }

    @Override
    public <T> List<Invoker<T>> route(List<Invoker<T>> list, URL url, Invocation invocation) throws RpcException {
        // 如果没有在重启列表中，才会加入到后续调用列表
        return list.stream().filter(c->
                !instance.hasRestartingInstance(c.getUrl().getParameter("remote.application"),c.getUrl().getIp()))
                .collect(Collectors.toList());
    }

    @Override
    public boolean isRuntime() {
        return false;
    }

    @Override
    public boolean isForce() {
        return false;
    }

    @Override
    public int getPriority() {
        return 0;
    }
}
